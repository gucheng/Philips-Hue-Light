import pygame
from Settings import *


class LightColor():

    def __init__(self, x, y, c):
        self.x = x
        self.y = y
        self.c = c
        self.r = 50

    def display(self, main):
        self.rect = pygame.draw.circle(
            main.screen, self.c, (self.x, self.y), self.r )

    # def mouseOver(self):
    #     mouseX, mouseY = pygame.mouse.get_pos()

    #     if(mouseX >= self.x - self.r and mouseX <= self.x + self.r):
    #         if(mouseY >= self.y - self.r and mouseY <= self.y + self.r):
    #             return True
    #     return False
