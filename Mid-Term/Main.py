import pygame
import sys
from Settings import *
from Switch import *
from transitions import Machine
from qhue import Bridge


class Core:
    states = ["Off", "Yellow", "Green", "Blue", "Purple", "Red"]

    def __init__(self):
        # initialize pygame and create window
        pygame.init()
        self.screen = pygame.display.set_mode((screenWidth, screenHeight))
        pygame.display.set_caption(title)
        self.clock = pygame.time.Clock()

        self.machine = Machine(model=self, states=Core.states, initial='Off')
        self.machine.add_transition(
            trigger='TurnYellow', source="Green", dest='Yellow')
        self.machine.add_transition(
            trigger='TurnBlue', source="Yellow", dest='Blue')
        self.machine.add_transition(
            trigger='TurnPurple', source="Blue", dest='Purple')
        self.machine.add_transition(trigger='TurnRed', source=[
                                    'Green', "Yellow", "Blue", "Purple"], dest='Red')
        self.machine.add_transition(trigger='TurnGreen', source=[
                                    'Off', "Purple", "Red"], dest='Green')

        self.lastTick = pygame.time.get_ticks()

    def Update(self):
        # Game Loop
        self.clock.tick(FPS)
        self.LightingUpdate()
        self.Events()
        self.Draw()

    def LightingUpdate(self):
        mousePress = pygame.mouse.get_pressed()[0]
        spacePress = pygame.key.get_pressed()[pygame.K_SPACE]
        bPress = pygame.key.get_pressed()[pygame.K_b]
        pPress = pygame.key.get_pressed()[pygame.K_p]
        gPress = pygame.key.get_pressed()[pygame.K_g]

        if(self.state == "Off"):
            self.TurnGreen()
            b.lights[2].state(on=True)
            print("On")

        if(self.state == "Green"):
            lc.c = green
            b.lights[2].state(hue=greenHue)

            if pygame.time.get_ticks() >= self.lastTick + 5000:
                self.lastTick = pygame.time.get_ticks()
                print("TurnYellow..")
                self.TurnYellow()

        if(self.state == "Yellow"):
            lc.c = yellow
            b.lights[2].state(hue=yellowHue)
            if(bPress):
                self.TurnBlue()
                print("TurnBlue")

        if(self.state == "Blue"):
            lc.c = blue
            b.lights[2].state(hue=blueHue)
            if(pPress):
                self.lastTick = pygame.time.get_ticks()
                self.TurnPurple()
                print("TurnPurple")

        if(self.state == "Purple"):
            lc.c = purple
            b.lights[2].state(hue=purpleHue)
            if pygame.time.get_ticks() <= self.lastTick + 20000:
                if (gPress):
                    self.lastTick = pygame.time.get_ticks()
                    self.TurnGreen()
                    print("TurnGreen")

        if(self.state == "Red"):
            lc.c = red
            b.lights[2].state(hue=redHue)
            if(gPress):
                self.lastTick = pygame.time.get_ticks()
                self.TurnGreen()
                print("TurnGreen")

        if(self.state == "Green" or self.state == "Yellow" or self.state == "Blue" or self.state == "Purple"):
            if(spacePress):
                self.TurnRed()
                print("TurnRed")

    def Events(self):
        # Process input(events)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

    def Draw(self):
        # Draw/render
        self.screen.fill(black)
        lc.display(self)

        # after drawing update the display
        pygame.display.update()

core = Core()

lc = LightColor(screenWidth // 2, screenHeight // 2, green)

b = Bridge("192.168.86.21", "nUS6rndMcOE0XIWIVsxMA8EA-0xJH4Yqk1A-hCDf")
while True:
    core.Update()
