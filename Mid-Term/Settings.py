# Game options/settings
screenWidth = 600
screenHeight = 300
title = "Philips Hue Light"
FPS = 60

# DefineColors
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 0)
purple = (208, 65, 244)
blue = (65, 149, 244)
yellow = (255, 255, 0)

redHue = 0
greenHue = 25500
purpleHue = 56100
blueHue = 46900
yellowHue = 12750
